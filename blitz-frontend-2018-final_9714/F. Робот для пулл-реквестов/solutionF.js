/*
type PullRequest = {  
     * Массив имён изменённых файлов (отсортирован лексикографически)  
     * Длина массива N: 1 <= N <= 1000  
    files: string[],  
     * Уникальный идентификатор реквеста в VCS  
    id: string,  
     * Unix-timestamp создания пулл-реквеста  
    created: number,  
}
*/
/**  
 * @param {PullRequest[]} pullRequests массив PR, отсортированных по времени создания  
 * @returns {string[]} идентификаторы реквестов в порядке мёржа  
 */  
module.exports = function (pullRequests) {  
	'use strict'
	function merge(requests) {
		const allfiles = new Set()
		const notconflict = []
		const conflict = []
		for (const request of requests) {
			const conflicts = request.files.filter((f) => allfiles.has(f))
			if (!conflicts.length) {
				request.files.forEach((f) => allfiles.add(f))
				notconflict.push(request)}
			else {
				conflict.push(request)}}
		return [notconflict, allfiles.size, conflict]}

	function combinations(items) {
		// XXX: I don't know algorithm, I used itertools.cominations
		// May be it is exact reason why I'm not yandex's employee
		// TODO: check for no conflict inside combination... when or if proper algorithm will be sleated.
		return items.map((i)=> [i])}

	const solutions = new Map()
	let maxWeight = 0
	const variants = [pullRequests]
	while (variants.length) {
		const variant = variants.shift()
		const [notconflict, weight, conflict] = merge(variant)
		if (!solutions.has(weight))
			solutions.set(weight, [])
		solutions.get(weight).push(notconflict)
		if (weight > maxWeight)
			maxWeight = weight
		for (const combination of combinations(conflict)) {
			const variantWithoutConflicts = []
			let i = 0
			for (const request of combination) {
				for (; i < variant.length; i++) {
					const candidate = variant[i]
					if (candidate.id == request.id) {
						variantWithoutConflicts.push(candidate)
						break}
					if (candidate.files.every((f) => !request.files.includes(f)))
						variantWithoutConflicts.push(candidate)}}
			const possibleCardinality = variantWithoutConflicts.reduce((accumulator, r) => accumulator + r.files.length, 0)
			if (possibleCardinality > maxWeight)
				variants.push(variantWithoutConflicts)}}

	return solutions.get(maxWeight)[0].map((r) => r.id)}
