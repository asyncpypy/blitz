const mergeAllPRs = require('./solutionF.js')

console.assert(  
    mergeAllPRs([  
        {  
            id: '#1',  
            created: 1536077100,  
            files: ['.gitignore', 'README.md']  
        },  
        {  
            id: '#2',  
            created: 1536077700,  
            files: ['index.js', 'package-lock.json', 'package.json']  
        },  
        {  
            id: '#3',  
            created: 1536077800,  
            files: ['.pnp.js', 'yarn.lock']  
        }  
    ])  
    .join(',') === [  
        "#1",  
        "#2",  
        "#3"  
    ].join(',')  
);  
 
console.assert(  
    mergeAllPRs([  
        {  
            id: '#1',  
            created: 1536074100,  
            files: ['README.md']  
        },  
        {  
            id: '#2',  
            created: 1536078700,  
            files: ['README.md']  
        },  
        {  
            id: '#3',  
            created: 1536097800,  
            files: ['README.md']  
        }  
    ]).join(',') === [  
        "#1"  
    ].join(',')  
);  
 
console.assert(  
    mergeAllPRs([  
        {  
            id: '#1',  
            created: 1536077100,  
            files: ['.gitignore', 'README.md']  
        },  
        {  
            id: '#2',  
            created: 1536077700,  
            files: ['index.js', 'package-lock.json', 'package.json']  
        },  
        {  
            id: '#3',  
            created: 1536077800,  
            files: ['.pnp.js', 'package-lock.json', 'yarn.lock']  
        },  
        {  
            id: '#4',  
            created: 1536077900,  
            files: ['index.spec.js', 'index.spec.ts', 'index.ts']  
        }  
    ])  
    .join(',') === [  
        "#1",  
        "#2",  
        "#4"  
    ].join(',')  
);
