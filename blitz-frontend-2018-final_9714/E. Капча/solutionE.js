/*
Обозначение объектов:

    S - дорожный знак (sign)
    T - дерево (tree)
    R - дорога (road)
    B - здание (building)
    С - автомобиль (car)
    A - животное (animal)
    W - водоем (water)
    H - человек (human)
*/
/**  
 * @param {captcha} строка с обозначениями
 * @returns {string[]} список строк прямоугольников, или пустой массив, если решения нет
 */  
module.exports = function solveCaptcha(captcha) {  
    // ...  
}
