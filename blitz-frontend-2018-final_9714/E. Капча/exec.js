const solver = require('./solutionE.js')

const captcha = `
  TRABWARH  
  THSCAHAW  
  WWBSCWAA  
  CACACHCR  
`

console.assert(solver(captcha),
	[  
  ‘TRABWARH  
   THSCAHAW‘  
   ,  
  ‘WWBSCWAA  
   CACACHCR‘  
])


console.assert(solver("TRABWARH\nTHSCAHAW\nWWBSCWAA\nCACACHCR"), ["TRABWARH\nTHSCAHAW","WWBSCWAA\nCACACHCR"])
console.assert(solver("CSRARHAR\nCWAHCBSW\nABWBSWBA\nRBSBTABH"), ["CSRARHAR","CWAHCBSW","ABWBSWBA","RBSBTABH"])
console.assert(solver("HSRSTBHC\nCAWTRTBT\nWBATSTRA\nTWRBRTRR\nRWTABSHB\nTWCBWBCA"), ["HS\nCA\nWB\nTW\nRW\nTW","RSTBHC\nWTRTBT","ATSTRA\nRBRTRR","TABSHB\nCBWBCA"])
console.assert(solver("TSRSBWAC\nASCSWBTC\nTTAHTABC\nAHWTRWWA"), [])
