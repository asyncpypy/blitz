const express = require('express');

const { BEEP_CODES } = require('@yandex-blitz/phone');

const createApp = ({ phone }) => {
    const app = express();

    // звонит по номеру записанному в "быстром наборе" под цифрой digit
    app.get("/speeddial/:digit", async (req, res) => {
        phone.getData().then(value => {
            const speeddialDict = JSON.parse(value);
            return phone.connect().then(() => {
                const number = speeddialDict[req.params.digit]
                if (typeof number !== 'number')
                    throw new Error('Not a number.');

                phone.dial(number);

                res.sendStatus(200);
            });
        }).catch((e) => {
            const code = {'Not a number.': BEEP_CODES.ERROR, 'unable to connect': BEEP_CODES.FATAL}[e.message]
            if (code !== undefined)
                phone.beep(code);

            res.sendStatus(500);
        });
    });

    // записывает в "быстрый набор" под цифру digit номер phonenumber
    let lock = Promise.resolve()
    app.post("/speeddial/:digit/:phonenumber", (req, res) => {
        lock = lock.then(() => phone.getData()
            .then(value => {
                const speeddialDict = JSON.parse(value);
                speeddialDict[req.params.digit] = Number(req.params.phonenumber);

                return phone.setData(JSON.stringify(speeddialDict))
                    .then(() => {
                        phone.beep(BEEP_CODES.SUCCESS);

                        res.sendStatus(200);
                    });
            })
            .catch(() => {
                phone.beep(BEEP_CODES.ERROR);

                res.sendStatus(500);
            }));
    });

    return app;
};

exports.createApp = createApp;
