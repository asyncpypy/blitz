/**
 * @param {Good|Comment} data - ссылка на товар, отзыв или ответ на отзыв,
 * из которой нужно восстановить все возможные данные
 * @return {string}
 */
module.exports = function (data) {
	'use strict'

	const goods = new Set()
	const comments = new Set()

	function walk(data, goods, comments) {
		if (data.type === 'good') {
			if (goods.has(data))
				return;
			goods.add(data)
			data.comments.forEach( item => walk(item, goods, comments))
			data.related.forEach( item => walk(item, goods, comments))
		} else if (data.type == 'comment') {
			if (comments.has(data))
				return;
			comments.add(data)
			data.comments.forEach( item => walk(item, goods, comments))
			walk(data.parent, goods, comments)}}
	walk(data, goods, comments)

	function compareComments(a, b) {
		if (a.text < b.text)
			return -1;
		if (a.text > b.text)
			return 1;
		return 0;}

	function compareGoods(a, b) {
		if (a.name < b.name)
			return -1;
		if (a.name > b.name)
			return 1;
		return 0;}

	const goodOutput = []
	for (const good of Array.from(goods).sort(compareGoods)) {
		goodOutput.push(`- ${good.name}`)
		for (const related of Array.from(good.related).sort(compareGoods)) {
			goodOutput.push(`  * ${related.name}`)}}


	function walkComments(comments, output, level) {
		const indent = '  '.repeat(level)
		for (const comment of comments.sort(compareComments)) {
			output.push(`${indent}- ${comment.text}`)
			walkComments(comment.comments, output, level+1)}}

	const commentOutput = []
	for (const comment of Array.from(comments).filter(c => c.parent.type === 'good').sort(compareComments)) {
		commentOutput.push(`- ${comment.text} - про ${comment.parent.name}`)
		walkComments(comment.comments, commentOutput, 1)}

	const result = 
`## Отзывы

${commentOutput.join('\n')}

## Товары

${goodOutput.join('\n')}
`
	return result;
}
