(function () {
	'use strict'

	const $game = document.querySelector('.game')
	const $symbols = $game.querySelectorAll('.target > .line > .symbol')
	const notes = 'A H C D E F G'.split(' ')
	const accidentals = ['flat', 'sharp']
	const octaves = notes.map(n => n+'1').concat(notes.map(n => n+'2'))
	let melody = []
	for (const $symbol of $symbols) {
		const klasses = Array.from($symbol.classList).filter(name => name !== 'symbol')
		let note = []
		for (const name of klasses) {
			if (octaves.includes(name)) {
				note.push(name)
				break}}

		if (note.length) {
			for (const name of klasses) {
				if (accidentals.includes(name)) {
					note.push(name)
					break}}
			melody.push(note)}}

	const note_number = new Map([
		['C1', 1],
		['C1 sharp', 2],
		['D1 flat', 2],
		['D1', 3],
		['D1 sharp', 4],
		['E1 flat', 4],
		['E1', 5],
		['F1', 6],
		['F1 sharp', 7],
		['G1 flat', 7],
		['G1', 8],
		['G1 sharp', 9],
		['A1 flat', 9],
		['A1', 10],
		['A1 sharp', 11],
		['H1 flat', 11],
		['H1', 12],
		['C2', 1+12],
		['C2 sharp', 2+12],
		['D2 flat', 2+12],
		['D2', 3+12],
		['D2 sharp', 4+12],
		['E2 flat', 4+12],
		['E2', 5+12],
		['F2', 6+12],
		['F2 sharp', 7+12],
		['G2 flat', 7+12],
		['G2', 8+12],
		['G2 sharp', 9+12],
		['A2 flat', 9+12],
		['A2', 10+12],
		['A2 sharp', 11+12],
		['H2 flat', 11+12],
		['H2', 12+12],])

	const $keys = $game.querySelector('.keys')

	const delay = 250
	function play(melody, note_number, $keys) {
		if (!melody.length)
			return;
		const note = melody.shift()
		const $key = $keys.querySelector(`.key:nth-child(${note_number.get(note.join(' '))})`)
		$key.click()
		setTimeout(play.bind(null, melody, note_number, $keys), delay)
	}
	setTimeout(play.bind(null, melody, note_number, $keys), delay)
})()
