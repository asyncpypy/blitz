/** @returns string */  
module.exports = function date(inputString) {
	const pattern = /ta'(so|ko|ta|qa|goo)[ ](\d+)/
	const match = inputString.toLowerCase().match(pattern)
	if (match === null)
		return '0';
	return match.slice(1, 3).join(' ')
}
