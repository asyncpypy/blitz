/***  
 * @param container {Node} ссылка на DOM-node контейнера  
 * @param str {string} строка с текстом предупреждения  
 * @param min {number} минимальное значение letter-spacing (целое число)  
 * @param max {number} максимальное значение letter-spacing (целое число)  
 * @return {number} значение letter-spacing (целое число, px) или null, если поместить предупреждение не удаётся  
 */  
function getLetterSpacing (container, str, min, max) {
	'use strict'
	function isOverflown(value) {
		container.style.letterSpacing = `${value}px`
		return container.scrollHeight > container.clientHeight || container.scrollWidth > container.clientWidth}

	const originText = container.innerText
	const originLS = container.style.letterSpacing
	container.innerText = str

	if (isOverflown(min)) {
		container.innerText = originText
		container.style.letterSpacing = originLS
		return null}

	let fit = min
	let test = max
	while (fit < test) {
		if (isOverflown(test)) {
			max = test
			test = fit + Math.floor((test-fit)/2)}
		else {
			fit = test
			test = max}}

	container.innerText = originText
	container.style.letterSpacing = originLS

	return fit}
